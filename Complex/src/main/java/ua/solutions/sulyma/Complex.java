package ua.solution.narozhnyy;

public class Complex {
	private final double re;
    private final double im;
 
    public Complex(double re, double im) {
        if (Double.isNaN(re) || Double.isNaN(im)) {
            throw new ArithmeticException();
        }
        this.re = re;
        this.im = im;
    }
 
    public double realPart()      { return re; }
    public double imaginaryPart() { return im; }
 
    public Complex add(Complex c) {
        return new Complex(re + c.re, im + c.im);
    }
 
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex))
            return false;
        Complex c = (Complex) o;
        return Double.compare(re, c.re) == 0 &&
               Double.compare(im, c.im) == 0;
    }
 
    @Override public int hashCode() {
        int result = 17 + Double.hashCode(re);
        result = 31 * result + Double.hashCode(im);
        return result;
    }
 
    @Override public String toString() {
        return "(" + re + (im < 0 ? "" : "+") + im + "i)";
    }
 
    public static void main(String[] args) {
        Complex z = new Complex(1, 2);
        System.out.println(z.add(z));
    }
}
